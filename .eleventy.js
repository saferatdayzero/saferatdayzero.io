const markdownIt = require("markdown-it");
const markdownItAnchor = require("markdown-it-anchor");
const pluginTOC = require("eleventy-plugin-nesting-toc");
const syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");

const { graphviz } = require("node-graphviz");

const jsdom = require("jsdom");
const { JSDOM } = jsdom;

module.exports = function (eleventyConfig) {
  eleventyConfig.setUseGitIgnore(false);
  eleventyConfig.addPassthroughCopy({
    static: "static",
    "node_modules/flowbite/dist/flowbite.js": "static/js/flowbite.js",
  });

  const md = markdownIt({
    html: true,
    linkify: true,
    typographer: true,
  }).use(markdownItAnchor, {});

  eleventyConfig.setLibrary("md", md);

  eleventyConfig.addFilter("readtime", function (value) {
    let noTags = value.replace(/<[^>]*>?/gm, "").replace(/\s+/gm, " ");
    return Math.round(noTags.split(" ").length / 200); // guess at average reading time
  });

  eleventyConfig.addPairedShortcode(
    "panel",
    async function (content, title, alert) {
      let classes = "rounded-lg p-4 ";
      if (alert === "info") {
        classes += "bg-ul-midnight-blue";
      } else {
        classes += "bg-gray-800";
      }

      let output = "";
      if (title) {
        output += `<p class="text-xl font-bold my-0">` + title + `</p>`;
      }
      output += md.render(content);
      return `<aside class="` + classes + `">` + output + `</aside>`;
    },
  );

  eleventyConfig.addPairedShortcode("markdown", async function (content) {
    return md.render(content);
  });

  eleventyConfig.addPairedShortcode(
    "digraph",
    async function (content, caption) {
      let graph =
        `
digraph cluster {
  bgcolor="transparent";
  node [style="filled", fillcolor="gray20", color="white", fontcolor="white"];
  edge [style="filled", fillcolor="gray20", color="white", fontcolor="white"];
` +
        content +
        `
}`;
      let svgfile = await graphviz.dot(graph, "svg");
      let dom = new JSDOM(svgfile);
      let svg = dom.window.document.querySelector("svg");
      svg.classList.add("mx-auto");

      let output =
        `<div class="w-full overflow-x-scroll py-4">` +
        svg.outerHTML +
        `</div>`;

      if (caption) {
        output +=
          `<figcaption class="text-lg text-center">` +
          caption +
          `</figcaption>`;
      }

      return `<figure class="my-0 w-full">` + output + `</figure>`;
    },
  );

  eleventyConfig.addPlugin(pluginTOC);
  eleventyConfig.addPlugin(syntaxHighlight);
};
