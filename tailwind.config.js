/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./*.{html,md}",
    "./.eleventy.js",
    "./**/*.{html,md}",
    "./node_modules/flowbite/**/*.js",
  ],
  theme: {
    extend: {
      colors: {
        ul: {
          "bright-blue": "#0A32FF",
          "dark-blue": "#000095",
          "midnight-blue": "#122C49",
          "steel-blue": "#00689C",
          "sky-blue": "#00CAFB",
          fog: "#577E9E",
          "light-blue": "#BCE4F7",
          "gray-4": "#58595b",
        },
      },
    },
  },
  plugins: [require("@tailwindcss/typography"), require("flowbite/plugin")],
};
