---
title: Manifesto
date: 2024-04-22
description: >-
    Introducing Safer@DayZero, what it is, what we're working towards.
authors:
    - brett
tags: ["meta", "devops", "infrastructure"]
---

**How can we be safer at day zero?**

As the Digital Safety Research Institute starts up, this is a question we've
thought about a lot recently. The reason? Because we're right in the middle of
it.

A "startup" is an organization that is operating in a state of
significant uncertainty. It might be a company, but it could also be a
non-profit, a research lab, a grant-funded project, or any other organization
with a need to navigate a period of uncertainty.

Intuitively, those of us who have worked in a startup have felt the pressure to
trade safety for business expedience. In this context, "safety" is often
communicated using terms like "quality", "cybersecurity", "robustness",
"testing", "validation", "compliance", and so on. Either way, "safety" is rarely
first, with terms like "speed", "agility", and "runway" taking center stage.

Here are some clues that you may be working in a startup organization:

-   Every new software purchase is scrutinized, regardless of price tag.

-   Accounts are shared to minimize the cost of seat licenses.

-   Administrator access is granted to everyone, or only the founder.

-   Equipment is purchased refurbished, or employees just bring equipment from home.

-   Multi-factor authentication is not configured or required, even when available.

-   The corporate network is a single consumer-grade wireless router.

-   Automated testing does not exist.

-   Ditto incident response, because there's no response team.

-   Ditto endpoint management, because there's no IT organization.

-   A corporate email service may exist, or it may just be a shared Gmail account.

-   Passwords are written on post-it notes attached to the systems they are used for.

-   Dashboards? There are no dashboards.

In this operating mode, important decisions are made quickly,
because runway, cash flow, and time to market are mortal business
considerations. Safety is a distant luxury that can be worried about when the
money comes in.

The problem is that, if the organization is successful, the culture of mortal
urgency becomes ingrained, and the resulting technical debt becomes a systemic
risk that haunts the organization for years to come. Migrations are difficult
and expensive, and poor choices are hard to untangle as the organizational
infrastructure and culture are increasingly built around them.

The risk this poses is not limited to the organization. Many organizations
accumulate sensitive data from a wide variety of places, from direct customer
relationships, to transaction records, to analytics systems. As the organization
grows, this data becomes more valuable, the organization's place in an ecosystem
becomes more critical, and the systems the organization operates become more
attractive targets for attackers.

It isn't hard to find examples of mistakes turning to catastrophe:

-   In 2018, [Apollo left a database publicly
    exposed](https://haveibeenpwned.com/PwnedWebsites#Apollo) without a password,
    compromising the names, phone numbers, email addresses, locations, employers,
    and job titles of roughly 126 million people.

-   In 2019, an [Audi third party vendor left data publicly
    exposed](https://haveibeenpwned.com/PwnedWebsites#Audi), compromising names,
    phone numbers, physical addresses, and vehicle information of about 3 million
    people. Additionally, in some cases, driver's licenses, dates of birth, and
    social security numbers were also exposed.

-   In 2020, [Covve left an Elasticsearch server publicly
    exposed](https://haveibeenpwned.com/PwnedWebsites#db8151dd), compromising the
    names, email addresses, phone numbers, physical addresses, and social media
    profiles of roughly 23 million people, including even people who had never
    used the service.

-   In 2014, [Snapchat failed to respond to numerous reported
    vulnerabilities](https://haveibeenpwned.com/PwnedWebsites#Snapchat), which led
    to the compromise of usernames and passwords of about 5 million people.

This is how the mistakes we make as organizations bring real harm to real people.

So how can we do better? Through our research, we seek to understand:

-   What tradeoffs do organizations make?

-   What circumstances lead to these tradeoffs being necessary?

-   What solutions can reduce or eliminate the need for these tradeoffs?

-   How can organizations adopt these solutions as early as possible on their
    journey, when resources are precious, time is scarce, and the future is
    uncertain?

We believe that by answering these questions, we can make startups everywhere
Safer at Day Zero.
